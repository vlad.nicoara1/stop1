package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	stop "gitlab.com/vlad.nicoara1/stop1/pkg/app/stop"
	"github.com/gorilla/mux"
)


func testHandler(w http.ResponseWriter, r *http.Request){
	fmt.Fprintf(w, "E ce trebuie")
}

type App struct {
	Address string
	Router  *mux.Router
}

func (a *App) ListenAndServe() error {
	if err := http.ListenAndServe(a.Address, a.Router); err != nil {
		log.Printf("HTTP server failed: %s", err)
		return err
	}
	return nil
}

func New(addr string) *App {
	return &App{
		Address: addr,
		Router:  mux.NewRouter(),
	}
}

func ContentTypeJson(next http.Handler) http.Handler {
	return http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Content-Type", "application/json")
			next.ServeHTTP(w, r)
		},
	)
}

func HandleTick(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.NotFound(w, r)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func main() {

	p, ok := os.LookupEnv("FUNCTIONS_CUSTOMHANDLER_PORT")
	if !ok {
		p = "8080"
	}
	a := New(fmt.Sprintf(":%s", p))
	a.Router.Use(ContentTypeJson)

	a.Router.HandleFunc("/pkg/app/stop", HandleTick)

}
