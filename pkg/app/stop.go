package stop

// Import key modules.
import (
	"context"
	"fmt"
	"os"
	"github.com/Azure/azure-sdk-for-go/sdk/azidentity"
	"github.com/Azure/azure-sdk-for-go/sdk/resourcemanager/containerservice/armcontainerservice"
)

var (
	subscriptionId    = os.Getenv("AZURE_SUBSCRIPTION_ID")
	location          = "North Europe"
	resourceGroupName = "testgolang2"
	shutdown 		  = [2]string{"4d5c62c0-9c6e-49f8-835a-674bfc40b15c", "8ac7b50c-7374-4a40-85bf-5951b3d691e9"}
	ctx               = context.Background()
	test 			  = [2]string{"17777a32-938a-414c-a7a6-6c856c904424", "4d5c62c0-9c6e-49f8-835a-674bfc40b15c"}
)

func main() {

	for _, subscriptionId := range shutdown {
		cred, err := azidentity.NewDefaultAzureCredential(nil)
		if err != nil {
			fmt.Print("Authentication failure: %+v", err)
		}
		
		client, err := armcontainerservice.NewManagedClustersClient(subscriptionId, cred, nil)
		if err != nil {
			fmt.Print("Authentication failure: %+v", err)
		}

		page := client.NewListPager(nil)

		for page.More(){
			nextResult, err := page.NextPage(ctx)
			if err != nil {
				fmt.Print("failed to advance page: %v", err)
			}
			for _, v := range nextResult.Value {
				resourceName := string(*v.Name)
				resourceGroup := string(*v.ID)[67:85] //toImprove
				fmt.Println("ALL GOOD")
				//client.BeginStop(ctx, resourceGroup, resourceName, nil)
			}
		}
	}
}
